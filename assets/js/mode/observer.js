define([

], function () {
    'use strict';
    // 售楼
    const event = {
        clientList: [],
        listen(key, fn) {
            if (!this.clientList[key]) {
                this.clientList[key] = [];
            }
            this.clientList[key].push(fn);
        },
        trigger(...rest) {
            let key = rest.shift();
            console.log(this.clientList);
            let fns = this.clientList[key];

            if (!fns || fns.length === 0) {
                return false;
            }
            fns.map(item => item.apply(this, rest));
        },
        remove(key, fn) {
            let fns = this.clientList[key];

            if (!fns) {
                return false;
            }
        }
    };

    const installEvent = function (obj) {
        for (let i in event) {
            obj[i] = event[i];
        }
    };

    const salesOffices = {};
    installEvent(salesOffices);

    salesOffices.listen('s88', (p) => console.log(p));
    salesOffices.listen('s120', (p) => console.log(p));

    salesOffices.trigger('s88', 20000);
    salesOffices.trigger('s120', 30000);


    // Dep
    class Dep {
        constructor() {
            this.subs = [];
        }

        addSub(sub) {
            this.subs.push(sub);
        }

        depend() {
            if (Dep.target) {
                Dep.target.addDep(this);
            }
        }
        notify() {
            let subs = this.subs.slice();
            subs.forEach(sub => sub.update());
        }
    }

    // Watcher
    class Watcher {
        constructor(vm, cb) {
            this.vm = vm;
            this.cb = cb;
            this.value = this.get();
        }

        get() {
            Dep.target = this;
            let value = this.getter.call(this.vm, this.vm);
            Dep.target = null;
            return value;

        }

        addDep(dep) {
            dep.addSub(this);
        }

        update() {
            this.run();
        }

        run() {
            let value = this.get();
            let oldValue = this.value;
            if (value !== oldValue) {
                this.value = value;
                this.cb.call(this.vm, value, oldValue);
            }
        }
    }

    class Observer {
        constructor(value) {
            this.value = value;
            this.walk(value);
        }

        walk(obj) {
            let keys = Object.keys(obj);
            for (let i = 0, l = keys.length; i < l; i++) {
                defineReactive(obj, keys[i], obj[keys[i]]);
            }
        }
    }

    function defineReactive(obj, key, val) {
        let dep = new Dep();
        Object.defineProperty(obj, key, {
            enumerable: true,
            configurable: true,
            get() {
                console.log('val------', val);
                if (Dep.target) {
                    dep.depend();
                }
                return val;
            },
            set(newVal) {
                if (val === newVal) {
                    return false;
                }
                console.log('监听到变化了');
                val = newVal;
                dep.notify();
            }
        });
    }

    function observe(value) {
        if (!value || typeof value !== 'object') {
            return false;
        }

        return new Observer(value);
    }

    Dep.target = null;
    const data = {name: 'hello'};

    observe(data);


    data.name = 'world';
});