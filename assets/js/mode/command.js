define([

], function () {
    'use strict';
    let button1 = document.getElementById('button1'),
        button2 = document.getElementById('button2'),
        button3 = document.getElementById('button3');

    const setCommand = function (btn, command) {
        btn.onclick = function () {
            command.execute();
        };
    };

    const menuBar = {
        refresh() {
            console.log('刷新菜单');
        }
    };

    const subMenu = {
        add() {
            console.log('增加子菜单');
        },
        del() {
            console.log('删除子菜单');
        }
    };

    const RefreshMenuBar = function (receiver) {
        this.receiver = receiver;
        if (typeof this.execute !== 'function') {
            RefreshMenuBar.prototype.execute = function () {
                this.receiver.refresh();
            };
        }
    };
});