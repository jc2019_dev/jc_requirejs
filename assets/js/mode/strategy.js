define([

], function () {
    'use strict';
    console.log('策略模式------------------');
    // 策略模式
    // 1.计算奖金

    const bonusStrategies = {
        S(salary) {
            return salary * 4;
        },
        A(salary) {
            return salary * 3;
        },
        B(salary) {
            return salary * 2;
        }
    };

    const calcBonus = function (level, salary) {
        return bonusStrategies[level](salary);
    };

    console.log(calcBonus('S', 20000));
    console.log(calcBonus('A', 10000));

    // 2.表单验证
    const formStrategies = {
        isNonEmpty(val, errorMsg) {
            if (val === '') {
                return errorMsg;
            }
        },

        isMobile(val, errorMsg) {
            if (!/(^1[3|5|8][0-9]{9}$)/.test(val)) {
                return errorMsg;
            }
        }
    };

    const validator = (function () {
        return {
            cache: [],
            add(val, rules) {
                for (let i = 0, l = rules.length; i < l; i++) {
                    let strategy = rules[i].strategy;
                    let errorMsg = rules[i].errorMsg;
                    this.cache.push(() => formStrategies[strategy](val, errorMsg));
                }
            },
            start() {
                for (let i = 0, l = this.cache.length; i < l; i++) {
                    let msg = this.cache[i]();
                    if (msg) {
                        return msg;
                    }
                }
            }
        };
    })();

    validator.add('231231231231', [{strategy: 'isNonEmpty', errorMsg: '手机号不能为空'}, {strategy: 'isMobile', errorMsg: '手机号格式错误'}]);

    console.log(validator.start());
});