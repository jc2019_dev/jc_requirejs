define([

], function () {
    'use strict';
    console.log('代理模式-------');
    // 代理模式
    // 1.小明送花
    class Flower {

    }

    let xiaoming = {
        sendFlower(target) {
            target.receiveFlower();
        }
    };

    let B = {
        receiveFlower() {
            A.listen(function () {
                let flower = new Flower();
                A.receiveFlower(flower);
            });

        }
    };
    let A = {
        receiveFlower(flower) {
            console.log('收到花----', flower);
        },
        listen(fn) {
            setTimeout(() => {
                fn();
            }, 10000);
        }
    };

    xiaoming.sendFlower(B);


    // 图片预加载

    const myImage = (function () {
        let imgNode = document.createElement('img');
        document.body.appendChild(imgNode);

        return function (src) {
            imgNode.src = src;
        };
    })();

    const proxyImage = (function () {
        let img = new Image;
        img.onload = function () {
            myImage(this.src);
        };

        return function (src) {
            myImage('./assets/img/giphy.webp');
            img.src = src;
        };
    })();

    proxyImage('./assets/img/1920x1080.jpg');

    // 合并http请求

    const syncFile = function (id) {
        console.log(`同步文件------${id}`);
    };

    const checkbox = document.querySelectorAll('input');
    console.log(checkbox);

    checkbox.forEach(item => {
        item.onclick = function () {
            if (this.checked) {
                proxySyncFile(item.id);
            }
        };
    });

    const proxySyncFile = (function () {
        let cache = [],
            timer = null;

        return function (id) {
            cache.push(id);
            if (timer) {
                return false;
            }

            timer = setTimeout(() => {
                syncFile(cache.join(','));
                clearTimeout(timer);
                timer = null;
            }, 5000);
        };
    })();


    const mult = function (...rest) {
        console.log('开始计算乘积');
        return rest.reduce((acc, cur) => acc * cur, 1);
    };

    console.log(mult(2, 3));
    console.log(mult(2, 3, 4));


    const proxyMult = (function () {
        let cache = {};
        return function (...rest) {
            let args = rest.join(',');
            if (cache[args]) {
                return cache[args];
            }
            return cache[args] = mult.apply(this, rest);
        };
    })();

    console.log(proxyMult(2,3,4));
    console.log(proxyMult(2,3,4));
    
    const createProxyFactory = function(fn) {
        let cache = {};
        return function(...rest) {
            let args = rest.join(',');
            if(cache[args]) {
                return cache[args];
            }
            return cache[args] = fn.apply(this,rest);
        };
    };
});