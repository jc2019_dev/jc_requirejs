define([

], function () {
    'use strict';
    // 单例模式
    console.log('单例模式-------------------');
    const Singleton = function (name) {
        this.name = name;
        if (typeof this.getName !== 'function') {
            Singleton.prototype.getName = function () {
                console.log(this.name);
            };
        }
    };

    Singleton.getInstance = (function () {
        let instance = null;
        return function (name) {
            if (!instance) {
                instance = new Singleton(name);
            }
            return instance;
        };
    })();


    let a = Singleton.getInstance('A');
    let b = Singleton.getInstance('B');
    console.log(a === b);


    // 透明单例
    const CreateDiv = function (html) {
        this.html = html;
        if (typeof this.init !== 'function') {
            CreateDiv.prototype.init = function () {
                let div = document.createElement('div');
                div.innerHTML = this.html;
                document.body.appendChild(div);
            };
        }
        this.init();
    };

    const ProxySingleton = (function () {
        let instance = null;
        return function (html) {
            if (!instance) {
                instance = new CreateDiv(html);
            }
            return instance;
        };
    })();


    let c = new ProxySingleton('C');
    let d = new ProxySingleton('D');
    console.log(c === d);

    // 惰性单例
    const createLoginLayer = function () {
        let div = document.createElement('div');
        div.innerHTML = '我是登录浮窗';
        div.style.display = 'none';
        document.body.appendChild(div);
        return div;
    };

    const getSingle = function (fn) {
        let res = null;
        return function (...rest) {
            console.log(this);
            return res || (res = fn.apply(this, rest));
        };
    };

    const createSingleLoginLayer = getSingle(createLoginLayer);

    document.querySelector('#loginBtn').onclick = function () {
        let loginLayer = createSingleLoginLayer();
        loginLayer.style.display = 'block';
    };
});